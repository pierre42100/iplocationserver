#!/bin/bash
cargo build --release

TEMP_DIR=$(mktemp -d)
cp target/release/ip-location-server "$TEMP_DIR"

docker build -f Dockerfile "$TEMP_DIR" -t pierre42100/ip_location_api

rm -r $TEMP_DIR

