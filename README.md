# IP Location Server
This rust project intends to provide an easy-to-use, 
self-updated API backend service to get approximate
location of any given IP address.

This project is based on the GeoLite database.

This project is a fork of the [geoip-rs crate](https://crates.io/crates/geoip-rs).

## License
This project is released under the GNU License.