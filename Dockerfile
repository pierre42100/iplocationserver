FROM debian:bookworm-slim

COPY ip-location-server /usr/local/bin/ip-location-server

ENTRYPOINT /usr/local/bin/ip-location-server
