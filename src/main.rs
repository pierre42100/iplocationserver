use ip_location_server::args::Args;
use ip_location_server::{server, updater};

#[actix_web::main]
async fn main() {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    if Args::get().is_open() {
        log::warn!("Starting server in open mode!");
    }

    if !updater::has_database() {
        log::info!("Initialize database...");
        updater::download_database()
            .await
            .expect("Failed to initialize database!");
        log::info!("Database ready!");
    }

    updater::load_database().expect("Failed to load database!");

    updater::start_update_loop().await;

    // Spawn HTTP server
    server::start().await;
}
