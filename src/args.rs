//! # Server configuration
use clap::Parser;
use std::path::Path;
use std::time::Duration;

/// IP location service API
#[derive(Parser, Debug)]
pub struct Args {
    /// Database update URL, to update the database in the MaxMind DBfile format
    ///
    /// Sample download URL:
    /// https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=LICENSE_KEY&suffix=tar.gz
    ///
    /// Get an access token from here : https://www.maxmind.com/en/accounts/current/geoip/downloads
    ///
    /// More instructions here : https://dev.maxmind.com/geoip/geoip-direct-downloads
    #[arg(long, env)]
    pub database_update_url: String,

    /// Database storage path (a file)
    ///
    /// The database is automatically downloaded on first launch
    ///
    /// This path must be writeable by the process of ip-location-server
    #[arg(long, env, default_value = "cities.mmdb")]
    database_storage_path: String,

    /// Database update frequency, in seconds
    #[arg(long, env, default_value_t = 604800)]
    database_update_frequency: u64,

    /// Listen address and port
    #[arg(long, short, env, default_value = "0.0.0.0:4000")]
    pub listen_address: String,

    /// Allowed clients origins
    ///
    /// Without any client, the service is open, and CORS header is set to "*"
    #[arg(long, short, env)]
    pub origins: Vec<String>,
}

lazy_static::lazy_static! {
    static ref ARGS: Args = {
        Args::parse()
    };
}

impl Args {
    /// Get parsed command line arguments
    pub fn get() -> &'static Args {
        &ARGS
    }

    /// Get database storage path
    pub fn db_path(&self) -> &Path {
        self.database_storage_path.as_ref()
    }

    /// Get database update frequency
    pub fn db_update_frequency(&self) -> Duration {
        Duration::from_secs(self.database_update_frequency)
    }

    /// Check whether the server is available in open mode (if any client can connect to it) or not
    pub fn is_open(&self) -> bool {
        self.origins.is_empty()
    }
}

#[cfg(test)]
mod test {
    use crate::args::Args;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Args::command().debug_assert()
    }
}
