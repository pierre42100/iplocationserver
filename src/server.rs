//! # Integrated server
//!
//! This code is taken from the geoip-rs crate, which contains the following license
//!
//! Copyright 2019 Federico Fissore
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//!   http://www.apache.org/licenses/LICENSE-2.0
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.

use std::net::{Ipv4Addr, Ipv6Addr};

use crate::args::Args;
use actix_web::http::header::HeaderMap;
use actix_web::App;
use actix_web::{web, HttpRequest, HttpResponse, HttpServer};
use maxminddb::geoip2::City;
use maxminddb::MaxMindDBError;
use serde::{Deserialize, Serialize};

use crate::updater;

#[derive(Serialize)]
struct NonResolvedIPResponse<'a> {
    pub ip_address: &'a str,
}

#[derive(Serialize)]
struct ResolvedIPResponse<'a> {
    pub ip_address: &'a str,
    pub latitude: &'a f64,
    pub longitude: &'a f64,
    pub postal_code: &'a str,
    pub continent_code: &'a str,
    pub continent_name: &'a str,
    pub country_code: &'a str,
    pub country_name: &'a str,
    pub region_code: &'a str,
    pub region_name: &'a str,
    pub province_code: &'a str,
    pub province_name: &'a str,
    pub city_name: &'a str,
    pub timezone: &'a str,
}

#[derive(Deserialize, Debug)]
struct QueryParams {
    ip: Option<String>,
    lang: Option<String>,
    callback: Option<String>,
}

fn ip_address_to_resolve(
    ip: Option<String>,
    headers: &HeaderMap,
    remote_addr: Option<&str>,
) -> String {
    ip.filter(|ip_address| {
        ip_address.parse::<Ipv4Addr>().is_ok() || ip_address.parse::<Ipv6Addr>().is_ok()
    })
    .or_else(|| {
        headers
            .get("X-Real-IP")
            .map(|s| s.to_str().unwrap().to_string())
    })
    .or_else(|| {
        remote_addr
            .map(|ip_port| ip_port.split(':').take(1).last().unwrap())
            .map(|ip| ip.to_string())
    })
    .expect("unable to find ip address to resolve")
}

fn get_language(lang: Option<String>) -> String {
    lang.unwrap_or_else(|| String::from("en"))
}

async fn index(req: HttpRequest, web::Query(query): web::Query<QueryParams>) -> HttpResponse {
    let cors_header = if Args::get().is_open() {
        "*".to_string()
    } else {
        let header = match req.headers().get("origin") {
            None => return HttpResponse::BadRequest().body("Missing Origin header!"),
            Some(header) => header,
        };

        let origin = String::from_utf8_lossy(header.as_bytes()).to_string();
        if !Args::get().origins.contains(&origin) {
            return HttpResponse::Unauthorized().body("Forbidden origin!");
        }

        origin
    };

    let language = get_language(query.lang);
    let ip_address =
        ip_address_to_resolve(query.ip, req.headers(), req.connection_info().peer_addr());

    let database = updater::get_database().unwrap();

    let lookup: Result<City, MaxMindDBError> = database.db().lookup(ip_address.parse().unwrap());

    let geoip = match lookup {
        Ok(geoip) => {
            let region = geoip
                .subdivisions
                .as_ref()
                .filter(|subdivs| !subdivs.is_empty())
                .and_then(|subdivs| subdivs.first());

            let province = geoip
                .subdivisions
                .as_ref()
                .filter(|subdivs| subdivs.len() > 1)
                .and_then(|subdivs| subdivs.get(1));

            let res = ResolvedIPResponse {
                ip_address: &ip_address,
                latitude: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.latitude.as_ref())
                    .unwrap_or(&0.0),
                longitude: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.longitude.as_ref())
                    .unwrap_or(&0.0),
                postal_code: geoip
                    .postal
                    .as_ref()
                    .and_then(|postal| postal.code.as_ref())
                    .as_ref()
                    .unwrap_or(&&""),
                continent_code: geoip
                    .continent
                    .as_ref()
                    .and_then(|cont| cont.code.as_ref())
                    .as_ref()
                    .unwrap_or(&&""),
                continent_name: geoip
                    .continent
                    .as_ref()
                    .and_then(|cont| cont.names.as_ref())
                    .and_then(|names| names.get(language.as_str()))
                    .as_ref()
                    .unwrap_or(&&""),

                country_code: geoip
                    .country
                    .as_ref()
                    .and_then(|country| country.iso_code.as_ref())
                    .as_ref()
                    .unwrap_or(&&""),
                country_name: geoip
                    .country
                    .as_ref()
                    .and_then(|country| country.names.as_ref())
                    .and_then(|names| names.get(language.as_str()))
                    .as_ref()
                    .unwrap_or(&&""),
                region_code: region
                    .and_then(|subdiv| subdiv.iso_code.as_ref())
                    .unwrap_or(&""),
                region_name: region
                    .and_then(|subdiv| subdiv.names.as_ref())
                    .and_then(|names| names.get(language.as_str()))
                    .as_ref()
                    .unwrap_or(&&""),
                province_code: province
                    .and_then(|subdiv| subdiv.iso_code.as_ref())
                    .as_ref()
                    .unwrap_or(&&""),
                province_name: province
                    .and_then(|subdiv| subdiv.names.as_ref())
                    .and_then(|names| names.get(language.as_str()))
                    .as_ref()
                    .unwrap_or(&&""),
                city_name: geoip
                    .city
                    .as_ref()
                    .and_then(|city| city.names.as_ref())
                    .and_then(|names| names.get(language.as_str()))
                    .as_ref()
                    .unwrap_or(&&""),
                timezone: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.time_zone.as_ref())
                    .as_ref()
                    .unwrap_or(&&""),
            };
            serde_json::to_string(&res)
        }
        Err(_) => serde_json::to_string(&NonResolvedIPResponse {
            ip_address: &ip_address,
        }),
    }
    .unwrap();

    match query.callback {
        Some(callback) => HttpResponse::Ok()
            .insert_header(("Access-Control-Allow-Origin", cors_header))
            .content_type("application/javascript; charset=utf-8")
            .body(format!(";{}({});", callback, geoip)),
        None => HttpResponse::Ok()
            .insert_header(("Access-Control-Allow-Origin", cors_header))
            .content_type("application/json; charset=utf-8")
            .body(geoip),
    }
}

pub async fn start() {
    let listen_address = &Args::get().listen_address;

    log::info!("Listening on http://{}", listen_address);

    HttpServer::new(move || App::new().route("/", web::route().to(index)))
        .bind(listen_address)
        .unwrap_or_else(|_| panic!("Can not bind to {}", listen_address))
        .run()
        .await
        .unwrap();
}
