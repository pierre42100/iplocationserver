//! # IP Location Server
//!
//! @author Pierre Hubert

pub mod args;
pub mod server;
pub mod updater;
pub mod utils;
