//! # Project utilities

use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};

pub type Res<E = ()> = Result<E, Box<dyn Error>>;

#[derive(Debug, Clone)]
pub struct ExecError(String);

impl ExecError {
    pub fn new(msg: &str) -> Self {
        Self(msg.to_string())
    }
}

impl Display for ExecError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Encountered error: {}", self.0)
    }
}

impl Error for ExecError {}

pub fn new_err(msg: &str) -> Res {
    Err(Box::new(ExecError(msg.to_string())))
}
