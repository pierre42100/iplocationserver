//! Database provider and updater

use std::io::Read;
use std::sync::{Arc, LockResult, Mutex, MutexGuard};

use crate::args::Args;
use flate2::read::GzDecoder;
use reqwest::header::CONTENT_TYPE;
use tar::Archive;

use crate::utils::{new_err, Res};

pub struct DatabaseWrapper(maxminddb::Reader<Vec<u8>>);

impl DatabaseWrapper {
    pub fn db(&self) -> &maxminddb::Reader<Vec<u8>> {
        &self.0
    }
}

static mut DATABASE: Option<Arc<Mutex<DatabaseWrapper>>> = None;

pub fn has_database() -> bool {
    return Args::get().db_path().is_file();
}

pub async fn download_database() -> Res {
    let response = reqwest::get(&Args::get().database_update_url).await?;
    let headers = response.headers();

    // Check response type
    let content_type = headers
        .get(CONTENT_TYPE)
        .ok_or("missing content-type header!")?;
    if !content_type.eq("application/gzip") && !content_type.eq("application/x-gzip") {
        return new_err(&format!(
            "Invalid Content-Type returned in response! (expected application/gzip, got {} !)",
            content_type.to_str()?
        ));
    }

    let bytes = response.bytes().await?;

    // Decompress response
    let reader = std::io::Cursor::new(bytes);
    let tar = GzDecoder::new(reader);
    let mut archive = Archive::new(tar);

    let mut buff = vec![];
    for entry in archive.entries()? {
        let mut entry = entry?;
        if entry.path()?.to_string_lossy().ends_with(".mmdb") {
            entry.read_to_end(&mut buff)?;
        }
    }

    if buff.is_empty() {
        return new_err("Could not find database in archive!");
    }

    std::fs::write(Args::get().db_path(), buff)?;

    Ok(())
}

pub fn load_database() -> Res {
    let bytes = std::fs::read(Args::get().db_path())?;
    let reader = maxminddb::Reader::from_source(bytes)?;

    unsafe {
        if DATABASE.is_none() {
            DATABASE = Some(Arc::new(Mutex::new(DatabaseWrapper(reader))));
        } else {
            DATABASE.as_ref().unwrap().lock()?.0 = reader;
        }
    }

    Ok(())
}

pub fn get_database() -> LockResult<MutexGuard<'static, DatabaseWrapper>> {
    unsafe { DATABASE.as_ref().unwrap().lock() }
}

async fn thread_loop() {
    loop {
        tokio::time::sleep(Args::get().db_update_frequency()).await;

        match download_database().await {
            Ok(_) => log::info!("Successfully updated city database !"),
            Err(e) => log::error!("Failed to update city database! {}", e),
        }

        if let Err(e) = load_database() {
            log::error!("Failed to reload database! {}", e);
        }
    }
}

pub async fn start_update_loop() {
    tokio::spawn(thread_loop());
}
